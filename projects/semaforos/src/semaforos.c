#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "led.h"
#include "switch.h"
#include "soc.h"

// Declaro variable global para que todas las tareas tengan acceso al semaforo
SemaphoreHandle_t semaforo;

// Nombres para los eventos del bit 0, 1, 2 y 3 de los eventos
#define TECLA_1 ( 1 << 0)
#define TECLA_2 ( 1 << 1)
#define TECLA_3 ( 1 << 2)
#define TECLA_4 ( 1 << 3)

void Rojo(void * parametros) {
   while(1) {
      Led_On(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
      Led_Off(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
   }
}

void Verde(void * parametros) {
   while(1) {
      xSemaphoreTake(semaforo, portMAX_DELAY);
      Led_On(GREEN_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
      Led_Off(GREEN_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
   }
}

void Teclas(void * parametros) {
   uint8_t tecla;
   while (1) {
      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla == NO_KEY);
            
      if (tecla == TEC1){
      }else if (tecla == TEC2){         
      }else if (tecla == TEC3){         
      }else if (tecla == TEC4){
         xSemaphoreGive(semaforo);
      }

      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla == NO_KEY);
      
   }   
}

int main(void) {
   /* Inicializaciones y configuraciones de dispositivos */
   SisTick_Init();
   Init_Leds();
   Init_Switches();

   semaforo = xSemaphoreCreateCounting(255, 0);
   if (semaforo != NULL){
      /* Creación de las tareas */
      xTaskCreate(Rojo, "Rojo", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      xTaskCreate(Verde, "Verde", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      xTaskCreate(Teclas, "Teclas", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      /* Arranque del sistema operativo */
      vTaskStartScheduler();
   }
   // Se prende Led Rojo cuando algun evento no se creo e indica error
   Led_On(RGB_R_LED);

   /* vTaskStartScheduler solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}
