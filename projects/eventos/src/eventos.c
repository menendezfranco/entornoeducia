#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "led.h"
#include "switch.h"
#include "soc.h"

// Declaro variable global para que todas las tareastengan acceso al event_group
EventGroupHandle_t eventos;

// Nombres para los eventos del bit 0, 1, 2 y 3 de los eventos
#define TECLA_1 ( 1 << 0)
#define TECLA_2 ( 1 << 1)
#define TECLA_3 ( 1 << 2)
#define TECLA_4 ( 1 << 3)

void Rojo(void * parametros) {
   while(1) {
      Led_On(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
      Led_Off(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
   }
}

void Verde(void * parametros) {
   while(1) {
      xEventGroupWaitBits(eventos, TECLA_4, pdFALSE, pdFALSE, portMAX_DELAY);
      Led_On(GREEN_LED);
      vTaskDelay(pdMS_TO_TICKS(2000));
      Led_Off(GREEN_LED);
      vTaskDelay(pdMS_TO_TICKS(2000));
      xEventGroupClearBits(eventos, TECLA_4);
   }
}

void Teclas(void * parametros) {
   uint8_t tecla;
   while (1) {
      tecla = Read_Switches();
      if (tecla != NO_KEY){
         Led_On(YELLOW_LED);
      } else {
         Led_Off(YELLOW_LED);
      }
      if (tecla == TEC1){
         xEventGroupSetBits(eventos, TECLA_1);
      }else if (tecla == TEC2){
         xEventGroupSetBits(eventos, TECLA_2);
      }else if (tecla == TEC3){
         xEventGroupSetBits(eventos, TECLA_3);
      }else if (tecla == TEC4){
         xEventGroupSetBits(eventos, TECLA_4);
      }
      vTaskDelay(pdMS_TO_TICKS(150));
   }   
}

int main(void) {
   /* Inicializaciones y configuraciones de dispositivos */
   SisTick_Init();
   Init_Leds();
   Init_Switches();

   eventos = xEventGroupCreate();
   if (eventos != NULL){
      /* Creación de las tareas */
      xTaskCreate(Rojo, "Rojo", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      xTaskCreate(Verde, "Verde", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      xTaskCreate(Teclas, "Teclas", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      /* Arranque del sistema operativo */
      vTaskStartScheduler();
   }
   // Se prende Led Rojo cuando algun evento no se creo e indica error
   Led_On(RGB_R_LED);

   /* vTaskStartScheduler solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}
