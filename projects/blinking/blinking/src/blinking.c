#include "FreeRTOS.h"
#include "task.h"
#include "led.h"
#include "switch.h"
#include "soc.h"

void Led(void * parametros) {
   while(1) {
      Led_On(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
      Led_Off(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
   }
}

void Teclas(void * parametros) {
   uint8_t tecla;
   while (1) {
      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla == NO_KEY);

      Led_On(GREEN_LED);

      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla != NO_KEY);

      Led_Off(GREEN_LED);
   }   
}

int main(void) {
   /* Inicializaciones y configuraciones de dispositivos */
   SisTick_Init();
   Init_Leds();
   Init_Switches();

   /* Creación de las tareas */
   xTaskCreate(Led, "Led", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
   xTaskCreate(Teclas, "Teclas", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);

   /* Arranque del sistema operativo */
   vTaskStartScheduler();

   /* vTaskStartScheduler solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}
