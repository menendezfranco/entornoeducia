#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "led.h"
#include "switch.h"
#include "soc.h"

typedef struct mensaje_s {
   uint8_t led;
   uint16_t tiempo;
} * mensaje_t;

QueueHandle_t cola;

void Rojo(void * parametros) {
   while(1) {
      Led_On(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
      Led_Off(RED_LED);
      vTaskDelay(pdMS_TO_TICKS(1000));
   }
}

void Consumidor(void * parametros) {
   struct mensaje_s mensaje;

   while(1) {
     xQueueReceive(cola, &mensaje, portMAX_DELAY);
     Led_On(mensaje.led);
     vTaskDelay(mensaje.tiempo);
     Led_Off(mensaje.led);
   }
}

// Tarea que escanea Teclas 1, 3 y 4
void Productor(void * parametros) {
   uint8_t tecla;
   struct mensaje_s mensaje;
    
   while (1) {
      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla == NO_KEY);
            
      if (tecla == TEC1){
         mensaje.led = RGB_B_LED;
         mensaje.tiempo = 500;
         xQueueSend(cola, &mensaje, portMAX_DELAY);
      }else if (tecla == TEC3){  
         mensaje.led = YELLOW_LED;
         mensaje.tiempo = 1500;
         xQueueSend(cola, &mensaje, portMAX_DELAY);      
      }else if (tecla == TEC4){
         mensaje.led = GREEN_LED;
         mensaje.tiempo = 2000;
         xQueueSend(cola, &mensaje, portMAX_DELAY);
      }

      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla == NO_KEY);
   }   
}
//Esta tarea solo escanea la tecla 2 (prioritaria)
void Tecla(void * parametros){
   uint8_t tecla;
   struct mensaje_s mensaje;

   while (1) {
      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla != TEC2);

      mensaje.led = RED_LED;
      mensaje.tiempo = 1000;
      //Envia al principio de la cola, se prioriza el led rojo en este caso en este ejemplo
      xQueueSendToFront(cola, &mensaje, portMAX_DELAY); 

      do {
         vTaskDelay(pdMS_TO_TICKS(150));
         tecla = Read_Switches();
      } while (tecla != NO_KEY); 
   }
}

int main(void) {
   /* Inicializaciones y configuraciones de dispositivos */
   SisTick_Init();
   Init_Leds();
   Init_Switches();

   cola = xQueueCreate(8, sizeof(struct mensaje_s));

   if (cola != NULL){
      /* Creación de las tareas */
      xTaskCreate(Productor, "Productor", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      xTaskCreate(Consumidor, "Consumidor", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
      xTaskCreate(Tecla, "Tecla", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
 
      /* Arranque del sistema operativo */
      vTaskStartScheduler();
   }
   // Se prende Led Rojo cuando no se creo correctamente la cola
   Led_On(RGB_R_LED);

   /* vTaskStartScheduler solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}
