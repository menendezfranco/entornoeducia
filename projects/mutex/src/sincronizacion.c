#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "semaphore.h"
#include "unt.h"
#include "soc.h"

SemaphoreHandle_t mutex;

void Dos(void * parametros) {
   while(1) {
      // Tomo el mutex para entrar en la seccion critica
      xSemaphoreTake(mutex, portMAX_DELAY);
      Escribir_Digito(2, 3);
      vTaskDelay(pdMS_TO_TICKS(1000));
      Escribir_Segmentos(0x00, 3);
      Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, 0x0F);
      // Devuelvo el mutex (recurso)
      xSemaphoreGive(mutex);
      vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

void Cinco(void * parametros) {
   while(1) {
      // Tomo el mutex para entrar en la seccion critica
      xSemaphoreTake(mutex, portMAX_DELAY);
      Escribir_Digito(5, 2);
      vTaskDelay(pdMS_TO_TICKS(1700));
      Escribir_Segmentos(0x00, 2);
      Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, 0x0F);
      // Devuelvo el mutex (recurso)
      xSemaphoreGive(mutex);
      vTaskDelay(pdMS_TO_TICKS(1700));
    }
}


int main(void) {
   /* Inicializaciones y configuraciones de dispositivos */
   SisTick_Init();
   Init_PonchoUNT();

   mutex = xSemaphoreCreateMutex();

   /* Creación de las tareas */
   xTaskCreate(Dos, "Dos", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
   xTaskCreate(Cinco, "Cinco", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
  

   /* Arranque del sistema operativo */
   vTaskStartScheduler();

   /* vTaskStartScheduler solo retorna si se detiene el sistema operativo */
   while(1);

   /* El valor de retorno es solo para evitar errores en el compilador*/
   return 0;
}
